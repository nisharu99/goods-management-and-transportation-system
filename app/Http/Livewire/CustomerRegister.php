<?php

namespace App\Http\Livewire;
use App\Models\Customer;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomerRegister extends Component
{
    public $message = '';

    public $new_customer_fname;
    public $new_customer_sname;
    public $new_customer_tp;
    public $new_customer_email;
    public $new_password;
    public $new_confirm_password;
    public $new_address;


    public function saveData()
    {
        $this->validate([

            'new_customer_fname' =>'required',
            'new_customer_sname' => 'required',
            'new_customer_email' => 'required|email|regex:/(.*)\./i|unique:customers,customer_email',
            'new_customer_tp' =>'required|min:10|:customers,customer_tp',
            'new_password' => 'required|min:6',
            'new_confirm_password' => 'required_with:new_password|same:new_password|min:6',
            'new_address' => 'required',

        ]);
        # code...
        $data = new Customer();
        $data -> customer_fname = $this->new_customer_fname;
        $data -> customer_sname = $this->new_customer_sname;
        $data -> customer_tp = $this->new_customer_tp;
        $data -> customer_email = $this->new_customer_email;
        $data -> password = Hash::make($this-> new_password);
        $data -> address = $this-> new_address;
        $data -> status = $this-> new_address;
        $data-> save();

        $this->clearData();

    }

    public function clearData()
    {
        # code...
        $this->new_customer_fname="";
        $this->new_customer_sname="";
        $this->new_customer_tp="";
        $this->new_customer_email="";
        $this->new_password="";
        $this->new_confirm_password ="";
        $this->new_address="";


    }

    public function render()
    {
        return view('livewire.customer-register')->layout('layouts.front');
    }
}
