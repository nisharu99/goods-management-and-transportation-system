<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Employee;
use Livewire\Component;

class EmployeeRegister extends Component
{
    public $message = '';

    public $new_employee_fname;
    public $new_employee_sname;
    public $new_employee_tp;
    public $new_employee_email;
    public $new_password;
    public $new_confirm_password;
    public $new_address = "0";
   
    public function saveData()
    {
        $this->validate([

            'new_employee_fname' =>'required',
            'new_employee_sname' => 'required',
            'new_employee_email' => 'required|email|regex:/(.*)\./i|unique:employees,employee_email',
            'new_employee_tp' =>'required|min:10|:employees,employee_tp',
            'new_password' => 'required|min:6',
            'new_confirm_password' => 'required_with:new_password|same:new_password|min:6',
            'new_address' => 'required',

        ]);
        # code...
        $data = new Employee();
        $data -> employee_fname = $this->new_employee_fname;
        $data -> employee_sname = $this->new_employee_sname;
        $data -> employee_tp = $this->new_employee_tp;
        $data -> employee_email = $this->new_employee_email;
        $data -> password = Hash::make($this-> new_password);
        $data -> address = $this-> new_address;
        $data-> save();

        $this->clearData();

    }

    public function clearData()
    {
        # code...
        $this->new_employee_fname="";
        $this->new_employee_sname="";
        $this->new_employee_tp="";
        $this->new_employee_email="";
        $this->new_password="";
        $this->new_confirm_password ="";
        $this->new_address="";


    }

    public function render()
    {
        return view('livewire.employee-register')->layout('layouts.front');
    }
}
