<?php

namespace App\Http\Livewire;

use Livewire\Component;

class GoodsComponent extends Component
{
    public function render()
    {
        return view('livewire.goods-component');
    }
}
